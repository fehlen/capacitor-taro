https://capacitorjs.com/docs/apis/status-bar#statusbarinforesult

# 安装步骤

- 1、安装 capacitor

```
npm install @capacitor/core @capacitor/cli
```

- 2、初始化项目,执行后会在目录中生成`capacitor.config.json`文件

```
npx cap init
```

- 3 添加平台

```
npx cap add android
npx cap add ios
```

# 启动步骤

- 1、 设置`capacitor.config.json` 中 server 配置对应机器的 IP，端口默认 10086

- 2、编辑命令行 `npx cap copy` 或者 `npx cap sync` 同步信息到`android`目录和`ios`目录

* 3、启动项目的 h5 编译

- 4、使用 `XCode` 或者 `Android Studio` 打开 `ios` 或者 `android` 目录，启动编译。即可看到效果。

<!--

"server": {
    "url": "http://192.168.2.105:10086",
    "cleartext": true
}

 -->
