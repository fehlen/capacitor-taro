import {View} from '@tarojs/components'
import {inject, observer} from '@tarojs/mobx'
import Taro, {Component} from '@tarojs/taro'
import {ComponentType} from 'react'
import NavBar from '../../components/Navbar'
import './index.scss'


type PageStateProps = {
  counterStore: {
    counter: number,
    increment: Function,
    decrement: Function,
    incrementAsync: Function
  }
}

interface Index {
  props: PageStateProps;
}

@inject('counterStore')
@observer
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lng: 5,
      lat: 34,
      zoom: 2
    };
  }

  async componentDidMount() {

  }
  handleClick(e) {
    console.log(e);
  }
  render() {
    return (
      <View className='index'>
        <NavBar title='首页'></NavBar>

      </View>
    )
  }
}

export default Index as ComponentType
