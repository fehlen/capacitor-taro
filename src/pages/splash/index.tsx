import {Plugins} from '@capacitor/core'
import {Text, View} from '@tarojs/components'
import {inject, observer} from '@tarojs/mobx'
import Taro, {Component} from '@tarojs/taro'
import {ComponentType} from 'react'
import NavBar from '../../components/Navbar'
import {AtNavBar} from 'taro-ui'
import './index.scss'

const safeAreaInsets = require('safe-area-insets')
const {StatusBar, Device, Network, App} = Plugins;


type PageStateProps = {
  counterStore: {
    counter: number,
    increment: Function,
    decrement: Function,
    incrementAsync: Function
  }
}

interface Index {
  props: PageStateProps;
}

@inject('counterStore')
@observer
class Index extends Component {
  state = {
    height: 0,
  }
  constructor() {
    super(...arguments)
  }

  componentWillMount() {

    setTimeout(() => {
      Taro.switchTab({url: '/pages/index/index'})
    }, 2000);

  }

  render() {
    return (
      <View className='index'>
        启动页面
      </View>
    )
  }
}

export default Index as ComponentType
