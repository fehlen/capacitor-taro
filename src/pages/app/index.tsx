import {Plugins} from '@capacitor/core'
import {Text, View} from '@tarojs/components'
import {inject, observer} from '@tarojs/mobx'
import Taro, {Component} from '@tarojs/taro'
import {ComponentType} from 'react'
import NavBar from '../../components/Navbar'
import {AtNavBar} from 'taro-ui'
import './index.scss'

const safeAreaInsets = require('safe-area-insets')
// const {StatusBar, Device, Network, App} = Plugins;


type PageStateProps = {
  counterStore: {
    counter: number,
    increment: Function,
    decrement: Function,
    incrementAsync: Function
  }
}

interface Index {
  props: PageStateProps;
}

@inject('counterStore')
@observer
class Index extends Component {

  state = {
    height: 0,
  }
  constructor() {
    super(...arguments)
  }
  onChange(files) {
    this.setState({
      files
    })
  }
  onFail(mes) {
    console.log(mes)
  }
  onImageClick(index, file) {
    console.log(index, file)


  }

  async componentWillMount() {
    // let status = await Network.getStatus();
    // console.log('status', status);
    // const statusbarInfo = await StatusBar.getInfo();
    // console.log('StatusBar=========', statusbarInfo.overlays);
    // console.log('taro', Taro.getSystemInfoSync());
    // const device = await Device.getInfo();
    // const android = device.platform;
    // if (android === 'android')
    //   this.setState({height: 24});
    // console.log('Device', device);
    // console.log("================================");
    // const appInfo = await App.getState();
    // console.log('AppInfo', appInfo);

    // Taro.showLoading({title: 'loading', mask: true});
    // setTimeout(() => {
    //   Taro.hideLoading();
    //   console.log('safeAreaInsets.support', safeAreaInsets.support)
    //   console.log('safe-area-inset-top', safeAreaInsets.top)
    //   console.log('safe-area-inset-left', safeAreaInsets.left)
    //   console.log('safe-area-inset-right', safeAreaInsets.right)
    //   console.log('safe-area-inset-bottom', safeAreaInsets.bottom)
    //   console.log('env', process.env.NODE_ENV)
    // }, 300);
  }

  handleClick(e) {
    console.log(e);
    // Taro.showModal({title: '提示', content: '删除'})
  }
  render() {
    const {height} = this.state;
    return (
      <View className='index'>
        <NavBar title='应用中心'></NavBar>
      </View>
    )
  }
}

export default Index as ComponentType
