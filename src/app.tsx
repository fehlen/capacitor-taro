import {DeviceInfo, Plugins, StatusBarStyle} from '@capacitor/core';
import {Provider} from '@tarojs/mobx';
import Taro, {Component, Config, getSystemInfoSync} from '@tarojs/taro';
import './app.scss';
import Index from './pages/index';
import counterStore from './store/counter';
const {StatusBar, Device} = Plugins;

if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5') {
  const VConsole = require("../assets/js/vconsole.min");
  new VConsole();
}
const store = {
  counterStore
}

class App extends Component {
  height: number = 0;
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    pages: [
      'pages/splash/index',
      'pages/index/index',
      'pages/mall/index',
      'pages/cart/index',
      'pages/user/index',
      'pages/app/index',
    ],
    tabBar: {
      custom: false,
      backgroundColor: 'white',
      color: "#bfbfbf",
      selectedColor: "#478ce9",
      borderStyle: "white",
      list: [
        {
          selectedIconPath: "assets/tab/tab1_checked.png",
          iconPath: "assets/tab/tab1.png",
          pagePath: "pages/index/index",
          // pagePath: "pages/appointment/index",
          text: "首页",
        },
        {
          selectedIconPath: "assets/tab/tab3_checked.png",
          iconPath: "assets/tab/tab3.png",
          pagePath: "pages/mall/index",
          text: "商城"
        },
        {
          selectedIconPath: "assets/tab/cart_checked.png",
          iconPath: "assets/tab/cart.png",
          pagePath: "pages/cart/index",
          text: "购物车"
        },
        {
          selectedIconPath: "assets/tab/order_checked.png",
          iconPath: "assets/tab/order.png",
          pagePath: "pages/app/index",
          text: "应用"
        },
        {
          selectedIconPath: "assets/tab/user_checked.png",
          iconPath: "assets/tab/user.png",
          pagePath: "pages/user/index",
          text: "用户"
        },
      ]
    },
  }

  isMobile: boolean = false;
  app: getSystemInfoSync.Result = Taro.getSystemInfoSync();
  device: DeviceInfo;

  // device = await Device.getInfo();
  async componentWillMount() {
    const startTime = new Date().getTime();
    this.device = await Device.getInfo();
    const platform = this.device.platform;
    this.height = platform === 'android' ? 24 : 0;
    this.isMobile = platform !== 'web' && platform !== 'electron';
    StatusBar.setStyle({
      style: StatusBarStyle.Dark,
    }).catch(error => {
      console.log('not mobile', error);
    });
    StatusBar.setOverlaysWebView({
      overlay: true
    }).catch(error => {
      console.log('not mobile', error);
    });
    const endTime = new Date().getTime();
    // console.log('end>', endTime);
    console.log('spend time>', endTime - startTime + 'ms');
  }


  appad = () => {
    console.log('appad>');
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
