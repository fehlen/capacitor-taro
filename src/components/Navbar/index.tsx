import {Text, View} from '@tarojs/components';
import Taro, {getApp} from '@tarojs/taro';
import {AtNavBar} from 'taro-ui';
import './index.scss';

interface Props {
  title?: string;
}


const NavBar = ({title}: Props) => {
  const {height} = getApp();
  console.log('getApp()>', getApp());
  function handleClick() { }
  return (
    <View className='navbar'>
      <AtNavBar
        customStyle={{paddingTop: `calc(env(safe-area-inset-top) + ${height}px)`, background: '#2d8bef !important'}}
        onClickRgIconSt={handleClick}
        onClickRgIconNd={handleClick}
        onClickLeftIcon={handleClick}
        leftIconType='home'
        // leftText={title}
        rightFirstIconType='share'
        rightSecondIconType='settings'
        fixed
        color='#fff'
        className='navbar'
        // @ts-ignore
        title={
          <Text style={{color: 'white'}}>{title}</Text>
        }
      />
    </View>

  )
}

export default NavBar
